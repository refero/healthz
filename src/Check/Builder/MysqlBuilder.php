<?php

namespace Healthz\Check\Builder;

use Healthz\Check\CheckInterface;
use Healthz\Check\Configuration\ConfigurationInterface;
use Healthz\Check\MysqlCheck;
use Healthz\Check\Result\ResultBuilder;
use Healthz\Helpers\PDOBuilder;
use Psr\Log\LoggerInterface;

class MysqlBuilder implements BuilderInterface
{
    /**
     * @var ResultBuilder
     */
    protected $resultBuilder;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(ResultBuilder $resultBuilder, ?LoggerInterface $logger = null)
    {
        $this->resultBuilder = $resultBuilder;
        $this->logger = $logger;
    }

    public function build(ConfigurationInterface $config): CheckInterface
    {
        return new MysqlCheck(
            $config,
            $this->resultBuilder,
            new PDOBuilder,
            $this->logger
        );
    }
}
