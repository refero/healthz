<?php

namespace Healthz\Tests\Check\Configuration;

use PHPUnit\Framework\TestCase;
use Healthz\Exception\InvalidBindingException;
use Healthz\Check\Configuration\MysqlConfiguration;
use Healthz\Check\Configuration\ConfigurationBuilder;
use Healthz\Check\Configuration\ConfigurationInterface;

class ConfigurationBuilderTest extends TestCase
{
    public function testConstruction()
    {
        $target = new ConfigurationBuilder;
        $this->assertInstanceOf(ConfigurationBuilder::class, $target);
    }

    public function testBuildValid()
    {
        $target = new ConfigurationBuilder;
        $result = $target->build(
            MysqlConfiguration::class, 
            [
                'host' => 'hosty',
                'port' => 3306,
                'user' => 'test',
                'pass' => 'pass',
                'dbname' => 'dbname',
            ]
        );
        $this->assertInstanceOf(MysqlConfiguration::class, $result);
    }

    public function testBuildInvalid()
    {
        $target = new ConfigurationBuilder;
       
        $this->expectException(InvalidBindingException::class);
        $this->expectExceptionMessage("doesnotexist does not implement " . ConfigurationInterface::class);

        $target->build(
            'doesnotexist', 
            [
                'blah' => 'meh'
            ]
        );
    }
}
