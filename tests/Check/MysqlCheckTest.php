<?php

namespace Healthz\Tests\Check;

use PHPUnit\Framework\TestCase;
use Healthz\Check\MysqlCheck;
use Healthz\Helpers\PDOBuilder;
use Healthz\Check\Result\Result;
use Healthz\Check\Result\ResultBuilder;
use Healthz\Check\Configuration\AbstractConfiguration;

class MysqlCheckTest extends TestCase
{
    public function testSuccessfulConstruction()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockPDOBuilder = $this->createMock(PDOBuilder::class);
        $mockPDOBuilder->expects($this->exactly(0))
            ->method('buildConnection')
            ->willReturn(null);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $check = new MysqlCheck($mockConfig, $mockResultBuilder, $mockPDOBuilder);
    }

    public function testSuccessfulRun()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockConfig->expects($this->exactly(9))
            ->method('get')
            ->will($this->returnValueMap([
                ['host', 'hosty'],
                ['dbname', 'dbdbdb'],
                ['port', '3306'],
                ['charset', null],
                ['unix_socket', null],
                ['query', 'SELECT * FROM here;'],
                ['user', 'some_user'],
                ['pass', 'some_pass'],
                ['timeout', 3],
            ]));

        $mockStatement = $this->createMock(\PDOStatement::class);
        $mockStatement->expects($this->exactly(1))
            ->method('fetch')
            ->willReturn(1);

        $mockPDO = $this->createMock(\PDO::class);
        $mockPDO->expects($this->exactly(1))
            ->method('query')
            ->with('SELECT * FROM here;')
            ->willReturn($mockStatement);

        $mockPDOBuilder = $this->createMock(PDOBuilder::class);
        $mockPDOBuilder->expects($this->exactly(1))
            ->method('buildConnection')
            ->with('mysql:host=hosty;dbname=dbdbdb;port=3306', 'some_user', 'some_pass')
            ->willReturn($mockPDO);

        $mockResult = $this->createMock(Result::class);

        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->willReturn($mockResult);

        $check = new MysqlCheck($mockConfig, $mockResultBuilder, $mockPDOBuilder);
        $this->assertEquals($mockResult, $check->run());
    }

    public function testConnectionException()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockConfig->expects($this->exactly(8))
            ->method('get')
            ->will($this->returnValueMap([
                ['host', 'hosty'],
                ['dbname', 'dbdbdb'],
                ['port', '3306'],
                ['charset', null],
                ['unix_socket', null],
                ['query', 'SELECT * FROM here;'],
                ['user', 'some_user'],
                ['pass', 'some_pass'],
                ['timeout', 3],
            ]));

        $mockStatement = $this->createMock(\PDOStatement::class);
        $mockStatement->expects($this->exactly(0))
            ->method('fetch')
            ->willReturn(1);

        $mockPDO = $this->createMock(\PDO::class);
        $mockPDO->expects($this->exactly(0))
            ->method('query')
            ->with('SELECT * FROM here;')
            ->willReturn($mockStatement);

        $expectedException = new \InvalidArgumentException("It died.");
        $mockPDOBuilder = $this->createMock(PDOBuilder::class);
        $mockPDOBuilder->expects($this->exactly(1))
            ->method('buildConnection')
            ->with('mysql:host=hosty;dbname=dbdbdb;port=3306', 'some_user', 'some_pass')
            ->willThrowException($expectedException);

        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(1))
            ->method('addError')
            ->with($expectedException->getMessage(), 'connection');
            
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->willReturn($mockResult);

        $check = new MysqlCheck($mockConfig, $mockResultBuilder, $mockPDOBuilder);
        $this->assertEquals($mockResult, $check->run());
    }

    public function testQueryException()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockConfig->expects($this->exactly(9))
            ->method('get')
            ->will($this->returnValueMap([
                ['host', 'hosty'],
                ['dbname', 'dbdbdb'],
                ['port', '3306'],
                ['charset', null],
                ['unix_socket', null],
                ['query', 'SELECT * FROM here;'],
                ['user', 'some_user'],
                ['pass', 'some_pass'],
                ['timeout', 3],
            ]));

        $mockStatement = $this->createMock(\PDOStatement::class);
        $mockStatement->expects($this->exactly(0))
            ->method('fetch')
            ->willReturn(1);

        $expectedException = new \InvalidArgumentException("It died.");
        $mockPDO = $this->createMock(\PDO::class);
        $mockPDO->expects($this->exactly(1))
            ->method('query')
            ->with('SELECT * FROM here;')
            ->willThrowException($expectedException);

        $mockPDOBuilder = $this->createMock(PDOBuilder::class);
        $mockPDOBuilder->expects($this->exactly(1))
            ->method('buildConnection')
            ->with('mysql:host=hosty;dbname=dbdbdb;port=3306', 'some_user', 'some_pass')
            ->willReturn($mockPDO);

        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(1))
            ->method('addError')
            ->with($expectedException->getMessage(), 'query');
            
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->willReturn($mockResult);

        $check = new MysqlCheck($mockConfig, $mockResultBuilder, $mockPDOBuilder);
        $this->assertEquals($mockResult, $check->run());
    }

    public function testFetchException()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockConfig->expects($this->exactly(9))
            ->method('get')
            ->will($this->returnValueMap([
                ['host', 'hosty'],
                ['dbname', 'dbdbdb'],
                ['port', '3306'],
                ['charset', null],
                ['unix_socket', null],
                ['query', 'SELECT * FROM here;'],
                ['user', 'some_user'],
                ['pass', 'some_pass'],
                ['timeout', 3],
            ]));

        $expectedException = new \InvalidArgumentException("It died.");
        $mockStatement = $this->createMock(\PDOStatement::class);
        $mockStatement->expects($this->exactly(1))
            ->method('fetch')
            ->willThrowException($expectedException);

        $mockPDO = $this->createMock(\PDO::class);
        $mockPDO->expects($this->exactly(1))
            ->method('query')
            ->with('SELECT * FROM here;')
            ->willReturn($mockStatement);

        $mockPDOBuilder = $this->createMock(PDOBuilder::class);
        $mockPDOBuilder->expects($this->exactly(1))
            ->method('buildConnection')
            ->with('mysql:host=hosty;dbname=dbdbdb;port=3306', 'some_user', 'some_pass')
            ->willReturn($mockPDO);

        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(1))
            ->method('addError')
            ->with($expectedException->getMessage(), 'query');
            
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->willReturn($mockResult);

        $check = new MysqlCheck($mockConfig, $mockResultBuilder, $mockPDOBuilder);
        $this->assertEquals($mockResult, $check->run());
    }
}
