# Healthz

PHP library to provide a consistent and simple healthcheck for a php application.

## Installation instructions

> This is not found on packagist...


### Composer 

Add this to the `repositories` section to your projects `composer.json`:

```
{
	"type": "vcs",
	"url": "https://gitlab.com/refero/healthz"
}
```

Add this to the `require` section of your composer.json:

```
"refero/healthz": "X.X.X"
```

> Use whichever version is most suitable.


Run the following command:

```
$ composer update refero/healthz
```

### Laravel

Register the Service provider in `config/app.php`:

```
'providers' => [
	...
	Healthz\Integration\Laravel\HealthzServiceProvider::class,
	...
]
```

Publish the configuration file to your application:

```
$ php artisan vendor:publish --provider="Healthz\Integration\Laravel\HealthzServiceProvider"
```

Now update the configuration to run the check suite you require.

### Lumen

Register the service provider in `bootstrap/app.php`

```
$app->register(Healthz\Integration\Lumen\HealthzServiceProvider::class);
```

Copy the config file found at `./src/Integration/Laravel/config.php` to`./config/healthz.php`.

> After installing the composer package you can run the following command from the root of your Lumen application:
```
$ cp ./vendor/refero/healthz/src/Integration/Larvavel/config.php ./config/healthz.php
```

Register the config file:

```
$app->configure('healthz');
```

Now update the configuration to run the check suite you require.

> You should use the `ConfigurationProvider`'s found in `./src/Integration/Laravel`...the usage doesn't change between Lumen and Laravel.