<?php

namespace Healthz\Check\Builder;

use Healthz\Check\CheckInterface;
use Healthz\Check\Configuration\ConfigurationInterface;
use Healthz\Check\RedisCheck;
use Healthz\Check\Result\ResultBuilder;
use Healthz\Helpers\RedisClientBuilder;
use Psr\Log\LoggerInterface;

class RedisBuilder implements BuilderInterface
{
    /**
     * ResultBuilder
     *
     * @var ResultBuilder
     */
    protected $resultBuilder;

    /**
     * @var null|LoggerInterface
     */
    protected $logger;

    public function __construct(ResultBuilder $resultBuilder, ?LoggerInterface $logger = null)
    {
        $this->resultBuilder = $resultBuilder;
        $this->logger = $logger;
    }

    public function build(ConfigurationInterface $config): CheckInterface
    {
        return new RedisCheck(
            $config,
            $this->resultBuilder,
            new RedisClientBuilder,
            $this->logger
        );
    }
}
