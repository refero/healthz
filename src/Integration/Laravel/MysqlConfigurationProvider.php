<?php

namespace Healthz\Integration\Laravel;

use Healthz\Check\Configuration\ConfigurationBuilder;
use Healthz\Check\Configuration\ConfigurationInterface;
use Healthz\Check\Configuration\MysqlConfiguration;
use Healthz\Integration\ConfigurationProviderInterface;

class MysqlConfigurationProvider implements ConfigurationProviderInterface
{
    /**
     * Retrieve the Configuration from the laravel config.
     * Using the laravel database.php config we extract the relevant credentials
     * for connecting to the MySQL database.
     *
     * There is 1 acceptable argument, 'connection' which defines which of the
     * connections configured in database.php are used for the check. The arguments
     * passed here should be defined in the healthz config.
     *
     * @param array $arguments
     *
     * @return ConfigurationInterface
     */
    public function retrieve(array $arguments = []): ConfigurationInterface
    {
        $connection = $arguments['connection'] ?? config('database.default');
        $dbParams = config('database.connections.' . $connection);

        if ($dbParams === null) {
            throw new InvalidConnectionException("There is no database connection called '" . $connection . "'.");
        }

        $configBuilder = new ConfigurationBuilder;

        return $configBuilder->build(MysqlConfiguration::class, [
            'host' => $dbParams['host'],
            'user' => $dbParams['username'],
            'pass' => $dbParams['password'],
            'dbname' => $dbParams['database'],
            'port' => $dbParams['port'],

            'timeout' => $arguments['timeout'] ?? 3,
        ]);
    }
}
