<?php

namespace Healthz\Tests\Check\Result;

use PHPUnit\Framework\TestCase;
use Healthz\Check\Result\Result;
use Healthz\Check\Result\ResultInterface;
use Healthz\Check\Configuration\AbstractConfiguration;

class ResultTest extends TestCase
{
    public function testResultConstruction()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $result = new Result($mockConfig);

        $this->assertInstanceOf(Result::class, $result);
        $this->assertInstanceOf(ResultInterface::class, $result);
    }

    public function testItIsSucessfulByDefault()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $result = new Result($mockConfig);

        $this->assertInstanceOf(Result::class, $result);
        $this->assertInstanceOf(ResultInterface::class, $result);
        $this->assertTrue($result->wasSuccessful());
    }

    public function testItIsUnsucessfulWhenAnErrorIsAdded()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $result = new Result($mockConfig);
        $result->addError('Some problem happened.', 'some_phase');

        $this->assertInstanceOf(Result::class, $result);
        $this->assertInstanceOf(ResultInterface::class, $result);
        $this->assertFalse($result->wasSuccessful());
    }

    public function testItReturnsErrorsThatWereAdded()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $result = new Result($mockConfig);
        $result->addError('Some problem happened.', 'some_phase');
        $result->addError('Some other problem happened.',);
        $expected = [
            ['message' => 'Some problem happened.', 'phase' => 'some_phase'],
            ['message' => 'Some other problem happened.', 'phase' => 'n/a'],
        ];
        $this->assertInstanceOf(Result::class, $result);
        $this->assertInstanceOf(ResultInterface::class, $result);
        $this->assertEquals($expected, $result->errors());
    }
}
