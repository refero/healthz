<?php

use Healthz\Check\Builder\MysqlBuilder;
use Healthz\Check\Builder\RedisBuilder;
use Healthz\Check\MysqlCheck;
use Healthz\Check\RedisCheck;
use Healthz\Integration\Laravel\MysqlConfigurationProvider;
use Healthz\Integration\Laravel\RedisConfigurationProvider;

return [
    // An array of all the builder classes to register with the package
    'builders' => [
        MysqlBuilder::class,
        RedisBuilder::class,
    ],

    // An array of all the configuration providers to register with the package
    'configurationProviders' => [
        MysqlConfigurationProvider::class,
        RedisConfigurationProvider::class,
    ],

    // Endpoint is the HTTP health check config
    'endpoint' => [

        // URI to use for the route
        'URI' => '_healthz',

        // Middleware groups to apply to the route
        'middleware' => [

        ],

        // The suite of checks to run during this test.
        'checks' => [

            // Example: Relation Database, MySQL.
            'rdb' => [

                /*
                 * The class that is responsible for running this Check.
                 * This one will connect and run a simple query against the database.
                */
                'class' => MysqlCheck::class,

                /*
                 * The configurationProvider is an option to allow you an easier
                 * integration with your wider application. You can use these if
                 * you cannot directly include the values in this file. This works
                 * equally well if you'd prefer to not duplicate env calls in here
                 * that are likely in the 'database.php' config anyway.
                 *
                 * This one will extract the creds for the 'default' connection,
                 * in the database.php config. You can also provide an argument
                 * of the connection name which you would like to test. If you
                 * use multiple databases, you can duplicate this config and just
                 * add a connection argument e.g.
                 *
                 *     'configurationProvider' => [
                 *         'class' => '...',
                 *         'arguments' => [
                 *             'connection' => 'mysql',
                 *         ]
                 *     ]
                 */
                'configurationProvider' => [
                    'class' => MysqlConfigurationProvider::class,
                ],

                /*
                 * The configuration block, defines the keys and values necessary
                 * to test the MySQL connection. Normally this would look like below
                 * using default laravel env() variables to extract the secrets
                 * from the environment.
                 *
                 *     'configuration' => [
                 *         'host' => env('DB_HOST', '127.0.0.1'),
                 *         'port' => env('DB_PORT', '3306'),
                 *         'dbname' => env('DB_DATABASE', 'forge'),
                 *         'user' => env('DB_USERNAME', 'forge'),
                 *         'pass' => env('DB_PASSWORD', ''),
                 *     ],
                */

            ],

            // Example: Cache DB, Redis.
            'cache' => [
                // @see above
                'class' => RedisCheck::class,

                /*
                 * @see above
                 *
                 * This will extract the creds for the 'cache' redis connection,
                 * in the database.php config. You could provide a different
                 * 'connection' argument if you'd like to test a different
                 * connection.
                 */
                'configurationProvider' => [
                    'class' => RedisConfigurationProvider::class,
                    'arguments' => [
                        'connection' => 'cache',
                    ],
                ],

                /*
                 * Example configuration block using default laravel env vars.
                 * @see above
                 *
                 *     'configuration' => [
                 *         'host' => env('REDIS_HOST', '127.0.0.1'),
                 *         'password' => env('REDIS_PASSWORD', null),
                 *         'port' => env('REDIS_PORT', 6379),
                 *         'database' => env('REDIS_CACHE_DB', 1),
                 *     ],
                 */
            ],

        ],
    ],

];
