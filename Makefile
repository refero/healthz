IMAGE_NAME=refero-healthz-test:local

.PHONY: test
test: 
	docker run -it -w /src -v "$(shell pwd):/src" --rm $(IMAGE_NAME) php ./vendor/bin/phpunit

.PHONY: composer-install
composer-install: 
	docker run -it -w /src -v "$(shell pwd):/src" --rm $(IMAGE_NAME) composer install

.PHONY: exec
exec:
	docker run -it -w /src -v "$(shell pwd):/src" --rm $(IMAGE_NAME) bash

.PHONY: cs
cs:
	docker run -it -w /src -v "$(shell pwd):/src" --rm $(IMAGE_NAME) php ./vendor/bin/php-cs-fixer fix

# Docker build
.PHONY: db
db:
	docker build -t $(IMAGE_NAME) .
