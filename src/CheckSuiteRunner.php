<?php

namespace Healthz;

class CheckSuiteRunner
{
    /**
     * @var CheckResolver
     */
    protected $checkResolver;

    /**
     * Constructor...
     *
     * @param CheckResolver $checkResolver
     */
    public function __construct(CheckResolver $checkResolver)
    {
        $this->checkResolver = $checkResolver;
    }

    /**
     * Loop through the suite definition, find and build all checks required, run them.
     * Return results of the checks.
     *
     * @param array $suiteDefinition
     *
     * @return array
     */
    public function run(array $suiteDefinition): array
    {
        $results = [];

        foreach ($suiteDefinition as $name => $checkDefinition) {
            $results[$name] = $this->checkResolver->resolve($checkDefinition)->run();
        }

        return $results;
    }
}
