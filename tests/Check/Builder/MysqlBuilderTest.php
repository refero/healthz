<?php

namespace Healthz\Tests\Check\Builder;

use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use Healthz\Check\MysqlCheck;
use Healthz\Check\Builder\MysqlBuilder;
use Healthz\Check\Result\ResultBuilder;
use Healthz\Check\Configuration\AbstractConfiguration;

class MysqlBuilderTest extends TestCase
{
    public function testConstruction()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $target = new MysqlBuilder($mockResultBuilder, $mockLogger);
        $this->assertInstanceOf(MysqlBuilder::class, $target);
    }

    public function testBuild()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $target = new MysqlBuilder($mockResultBuilder, $mockLogger);
        $this->assertInstanceOf(MysqlBuilder::class, $target);
        $this->assertInstanceOf(MysqlCheck::class, $target->build($mockConfig));
    }
}
