<?php

namespace Healthz\Check\Configuration;

use Healthz\Redis\ClientWrapper;

class RedisConfiguration extends AbstractConfiguration
{
    /**
     * Checks required keys are present.
     *
     *
     * @param array $config
     *
     * @throws Healthz\Exception\InvalidConfigurationException
     *
     * @return void
     */
    protected function guard(array $config)
    {
        $this->guardRequiredKeys(
            [
                'host',
                'port',
                'password',
                'database',
            ],
            $config
        );
    }

    /**
     * Add the optional config options if they are not present.
     *
     * @param array $config
     *
     * @return array
     */
    protected function decorate(array $config): array
    {
        if (!isset($config['timeout'])) {
            $config['timeout'] = 3.0;
        }

        if (!isset($config['client'])) {
            $config['client'] = ClientWrapper::PHPREDIS_CLIENT_NAME;
        }

        return $config;
    }
}
