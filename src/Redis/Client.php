<?php

namespace Healthz\Redis;

interface Client
{
    public function connect();

    public function ping();
}
