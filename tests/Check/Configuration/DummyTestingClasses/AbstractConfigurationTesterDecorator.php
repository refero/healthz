<?php

namespace Healthz\Tests\Check\Configuration\DummyTestingClasses;

use Healthz\Tests\Check\Configuration\DummyTestingClasses\AbstractConfigurationTester;

class AbstractConfigurationTesterDecorator extends AbstractConfigurationTester
{
    /**
     * Add a key to the config, just to check the decorator is run.
     *
     * @return void
     */
    protected function decorate(array $config): array
    {
        $config['__decorate'] = 'decorated';
        return $config;
    }
}