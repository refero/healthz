<?php

use Healthz\Integration\Lumen\HealthzController;

/*
 * Healthz route
 *
 * The route URI can be defined in the config but will default to '/_healthz'.
 */
Route::group([], function () {
    Route::get(
        '/' . config('healthz.endpoint.URI', '_healthz'),
        HealthzController::class . '@check'
    );
});
