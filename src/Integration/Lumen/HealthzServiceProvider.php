<?php

namespace Healthz\Integration\Lumen;

use Healthz\Integration\Laravel\HealthzServiceProvider as LaravelServiceProvider;

class HealthzServiceProvider extends LaravelServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }
}
