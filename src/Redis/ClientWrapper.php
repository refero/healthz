<?php

namespace Healthz\Redis;

use Healthz\Exception\UnknownRedisClientTypeException;
use Predis\Client as PRedisClient;

class ClientWrapper implements Client
{
    public const PREDIS_CLIENT_NAME = 'predis';

    public const PHPREDIS_CLIENT_NAME = 'phpredis';

    public const AVAILABLE_TYPES = [
        self::PHPREDIS_CLIENT_NAME,
        self::PREDIS_CLIENT_NAME,
    ];

    protected array $config;

    protected string $clientType;

    /**
     * @var PRedisClient|\Redis
     */
    protected $client;

    public function __construct(array $config, string $clientType)
    {
        $this->config = $config;
        $this->clientType = $clientType;
        $this->client = $this->buildClient();
    }

    public function connect()
    {
        switch ($this->clientType) {
            case self::PREDIS_CLIENT_NAME:
                $this->client->connect();
                break;
            case self::PHPREDIS_CLIENT_NAME:
                $this->client->connect($this->config['host'], $this->config['port']);
                $this->client->auth($this->config['password']);
                break;
        }
    }

    public function ping()
    {
        switch ($this->clientType) {
            // Both clients share the same interface for this method
            case self::PREDIS_CLIENT_NAME:
            case self::PHPREDIS_CLIENT_NAME:
                $this->client->ping();
                break;
        }
    }

    protected function guardRedisClientType()
    {
        if (!in_array($this->clientType, self::AVAILABLE_TYPES)) {
            throw new UnknownRedisClientTypeException("Unknown redis client type: $this->clientType");
        }
    }

    protected function buildClient()
    {
        $this->guardRedisClientType();

        switch ($this->clientType) {
            case self::PHPREDIS_CLIENT_NAME:
                return new \Redis;
            case self::PREDIS_CLIENT_NAME:
                return new PRedisClient($this->config);
        }

        // Shouldn't be possible to reach this, due to the guard before the switch, but belt & braces...
        throw new UnknownRedisClientTypeException("Unknown redis client type: $this->clientType");
    }
}
