<?php

namespace Healthz\Helpers;

class PDOBuilder
{
    /**
     * Builds the PDO connection object.
     *
     * Just abstracts away the instantiation from the Check itself.
     * Makes it alot easier to separate the External requirements from the logic.
     *
     * @param string     $dsn
     * @param string     $user
     * @param string     $password
     * @param null|array $pdoParams
     *
     * @return \PDO
     */
    public function buildConnection(string $dsn, string $user, string $password, ?array $pdoParams = null): ?\PDO
    {
        return new \PDO($dsn, $user, $password, $pdoParams);
    }
}
