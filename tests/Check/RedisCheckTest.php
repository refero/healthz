<?php

namespace Healthz\Tests\Check;

use Healthz\Check\RedisCheck;
use PHPUnit\Framework\TestCase;
use Healthz\Check\Result\Result;
use Healthz\Redis\ClientWrapper;
use Healthz\Check\Result\ResultBuilder;
use Healthz\Helpers\RedisClientBuilder;
use Healthz\Check\Configuration\AbstractConfiguration;

class RedisCheckTest extends TestCase
{
    public function testSuccessfulConstruction()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockPDOBuilder = $this->createMock(RedisClientBuilder::class);
        $mockPDOBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $check = new RedisCheck($mockConfig, $mockResultBuilder, $mockPDOBuilder);
    }

    public function testSuccessfulRun()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockConfig->expects($this->exactly(5))
            ->method('get')
            ->will($this->returnValueMap([
                ['host', 'hosty'],
                ['password', 'testy'],
                ['port', '6379'],
                ['client', ClientWrapper::PHPREDIS_CLIENT_NAME],
            ]));

            
        $mockRedisClient = $this->createMock(\Healthz\Redis\Client::class);
        $mockRedisClient->expects($this->exactly(1))
            ->method('connect');
        $mockRedisClient->expects($this->exactly(1))
                ->method('ping');

        $mockRedisClientBuilder = $this->createMock(RedisClientBuilder::class);
        $mockRedisClientBuilder->expects($this->exactly(1))
            ->method('build')
            ->with(['host' => 'hosty', 'port' => '6379', 'password' => 'testy', 'timeout' => null])
            ->willReturn($mockRedisClient);

        $mockResult = $this->createMock(Result::class);

        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->willReturn($mockResult);

        $check = new RedisCheck($mockConfig, $mockResultBuilder, $mockRedisClientBuilder);
        $this->assertEquals($mockResult, $check->run());
    }

    public function testConnectionException()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockConfig->expects($this->exactly(5))
            ->method('get')
            ->will($this->returnValueMap([
                ['host', 'hosty'],
                ['password', 'testy'],
                ['port', '6379'],
                ['client', ClientWrapper::PHPREDIS_CLIENT_NAME],
            ]));

        $expectedException = new \InvalidArgumentException("It died.");
        $mockRedisClient = $this->createMock(\Healthz\Redis\Client::class);
        $mockRedisClient->expects($this->exactly(1))
            ->method('connect')
            ->willThrowException($expectedException);

        $mockRedisClient->expects($this->exactly(0))
                ->method('ping');

        $mockRedisClientBuilder = $this->createMock(RedisClientBuilder::class);
        $mockRedisClientBuilder->expects($this->exactly(1))
            ->method('build')
            ->with(['host' => 'hosty', 'port' => '6379', 'password' => 'testy', 'timeout' => null])
            ->willReturn($mockRedisClient);

        $mockResult = $this->createMock(Result::class);
    $mockResult->expects($this->exactly(1))
        ->method('addError')
        ->with($expectedException->getMessage(), 'connection');

        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->willReturn($mockResult);

        $check = new RedisCheck($mockConfig, $mockResultBuilder, $mockRedisClientBuilder);
        $this->assertEquals($mockResult, $check->run());
    }

    public function testPingException()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockConfig->expects($this->exactly(5))
            ->method('get')
            ->will($this->returnValueMap([
                ['host', 'hosty'],
                ['password', 'testy'],
                ['port', '6379'],
                ['client', ClientWrapper::PHPREDIS_CLIENT_NAME],
            ]));

        $expectedException = new \InvalidArgumentException("It died.");
        $mockRedisClient = $this->createMock(\Healthz\Redis\Client::class);
        $mockRedisClient->expects($this->exactly(1))
            ->method('connect');

        $mockRedisClient->expects($this->exactly(1))
                ->method('ping')
                ->willThrowException($expectedException);

        $mockRedisClientBuilder = $this->createMock(RedisClientBuilder::class);
        $mockRedisClientBuilder->expects($this->exactly(1))
            ->method('build')
            ->with(['host' => 'hosty', 'port' => '6379', 'password' => 'testy', 'timeout' => null])
            ->willReturn($mockRedisClient);

        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(1))
            ->method('addError')
            ->with($expectedException->getMessage(), 'ping');

        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->willReturn($mockResult);

        $check = new RedisCheck($mockConfig, $mockResultBuilder, $mockRedisClientBuilder);
        $this->assertEquals($mockResult, $check->run());
    }
}
