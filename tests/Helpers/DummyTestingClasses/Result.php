<?php

namespace Healthz\Tests\Helpers\DummyTestingClasses;

use Healthz\Check\Result\ResultInterface;
use Healthz\Check\Configuration\ConfigurationInterface;

class Result implements ResultInterface
{
    public function __construct(ConfigurationInterface $config)
    {

    }

    public function wasSuccessful(): bool
    {
        return true;
    }

    public function errors(): array
    {
        return [];
    }

    public function addError(string $message, string $phase)
    {
        
    }
}