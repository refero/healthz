<?php

namespace Healthz\Check\Result;

use Healthz\Check\Configuration\ConfigurationInterface;

class ResultBuilder
{
    /**
     * Create a new Result object.
     *
     * @param ConfigurationInterface $config
     *
     * @return ResultInterface
     */
    public function build(ConfigurationInterface $config): ?ResultInterface
    {
        return new Result($config);
    }
}
