<?php

namespace Healthz\Exception;

class UnknownRedisClientTypeException extends \Exception
{
}
