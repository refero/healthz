<?php

namespace Healthz\Check\Configuration;

use Healthz\Exception\InvalidBindingException;

/**
 * Builds configurations for use with the Healthz package.
 * Works as a basic factory with some validation about the classes given.
 */
class ConfigurationBuilder
{
    /**
     * Builds a Configuration object.
     * This can be any class that implements ConfigurationInterface.
     * They should only require an array as an argument.
     * If a class is given that does not implement ConfigurationInterface, you will get an exception.
     *
     *
     * @param string $class
     * @param array  $config
     *
     * @throws InvalidBindingException
     *
     * @return ConfigurationInterface
     */
    public function build(string $class, array $config): ConfigurationInterface
    {
        if (!$this->implementsConfigurationInterface($class)) {
            throw new InvalidBindingException("$class does not implement " . ConfigurationInterface::class);
        }

        $config = new $class($config);

        return $config;
    }

    /**
     * Checks if a class implements ConfigurationInterface.
     * This does not instantiate an object but works only on the class path.
     * There may be issues during the construction of the configuration.
     * Relying on a successful construction would make it slightly more difficult to validate.
     *
     * @param string $class
     *
     * @return bool
     */
    protected function implementsConfigurationInterface(string $class): bool
    {
        try {
            $implements = class_implements($class);
        } catch (\Throwable $e) {
            return false;
        }

        if (in_array(ConfigurationInterface::class, $implements)) {
            return true;
        }

        return false;
    }
}
