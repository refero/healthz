<?php

namespace Healthz\Tests\Helpers;

use PHPUnit\Framework\TestCase;
use Healthz\Helpers\PDOBuilder;

/**
 * @README
 *
 * This is as extensively as we can test this class.
 * The PDO object creates a connection to the database required upon instantiation.
 * This would require integration testing; not unit testing.
 */
class PDOBuilderTest extends TestCase
{
    public function testSuccessfulConstruction()
    {
        $target = new PDOBuilder;
        $this->assertInstanceOf(PDOBuilder::class, $target);
    }
}
