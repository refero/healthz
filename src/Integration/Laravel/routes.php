<?php



/**
 * Healthz route
 *
 * The route URI can be defined in the config but will default to '/_healthz'.
 */
Route::namespace('Healthz\Integration\Laravel') // Should get the namespace from the class itself - hard coded is baaad
    ->as('healthz.')
    ->name('check')
    ->group(function () {
        Route::get(
            '/' . config('healthz.endpoint.URI', '_healthz'),
            'HealthzController@check'
        );
    });
