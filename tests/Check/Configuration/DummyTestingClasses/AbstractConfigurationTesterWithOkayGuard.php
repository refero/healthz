<?php

namespace Healthz\Tests\Check\Configuration\DummyTestingClasses;

use Healthz\Tests\Check\Configuration\DummyTestingClasses\AbstractConfigurationTester;

class AbstractConfigurationTesterWithOkayGuard extends AbstractConfigurationTester
{
    /**
     * Purposely just throw an exception to test it is used in the constructor.
     *
     * @return void
     */
    protected function guard(array $config)
    {
        if (! isset($config['im_required'])) {
            throw new \InvalidArgumentException("bad data");
        }
    }
}