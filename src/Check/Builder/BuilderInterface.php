<?php

namespace Healthz\Check\Builder;

use Healthz\Check\CheckInterface;
use Healthz\Check\Configuration\ConfigurationInterface;

interface BuilderInterface
{
    public function build(ConfigurationInterface $config): CheckInterface;
}
