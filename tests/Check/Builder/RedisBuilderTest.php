<?php

namespace Healthz\Tests\Check\Builder;

use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use Healthz\Check\RedisCheck;
use Healthz\Check\Builder\RedisBuilder;
use Healthz\Check\Result\ResultBuilder;
use Healthz\Check\Configuration\AbstractConfiguration;

class RedisBuilderTest extends TestCase
{
    public function testConstruction()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $target = new RedisBuilder($mockResultBuilder, $mockLogger);
        $this->assertInstanceOf(RedisBuilder::class, $target);
    }

    public function testBuild()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $target = new RedisBuilder($mockResultBuilder, $mockLogger);
        $this->assertInstanceOf(RedisBuilder::class, $target);
        $this->assertInstanceOf(RedisCheck::class, $target->build($mockConfig));
    }
}
