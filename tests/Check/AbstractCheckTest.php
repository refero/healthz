<?php

namespace Healthz\Tests\Check;

use PHPUnit\Framework\TestCase;
use Healthz\Check\AbstractCheck;
use Healthz\Check\Result\Result;
use Healthz\Check\Result\ResultBuilder;
use Healthz\Check\Result\ResultInterface;
use Healthz\Check\Configuration\AbstractConfiguration;

class AbstractCheckTester extends AbstractCheck
{
    public function run(): ResultInterface
    {
        return $this->resultBuilder->build($this->config);
    }

    public static function requiredConfiguration(): string
    {
        return 'config-path';
    }

    public static function requiredBuilder(): string
    {
        return 'builder-path';
    }
}

class AbstractCheckTest extends TestCase
{
    public function testSuccessfulConstructon()
    {
        $mockResult = $this->createMock(Result::class);
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $check = new AbstractCheckTester($mockConfig, $mockResultBuilder);
    }

    public function testItReturnsTheCorrectResult()
    {
        $mockResult = $this->createMock(Result::class);
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->with($mockConfig)
            ->willReturn($mockResult);

        $check = new AbstractCheckTester($mockConfig, $mockResultBuilder);
        $this->assertEquals($mockResult, $check->run());
    }
}
