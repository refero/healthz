<?php

namespace Healthz\Tests\Helpers;

use Healthz\Redis\Client;
use PHPUnit\Framework\TestCase;
use Healthz\Redis\ClientWrapper;
use Healthz\Helpers\RedisClientBuilder;

class RedisClientBuilderTest extends TestCase
{
    public function testSuccessfulConstruction()
    {
        $target = new RedisClientBuilder;
        $this->assertInstanceOf(RedisClientBuilder::class, $target);
    }

    public function testBuild()
    {
        $target = new RedisClientBuilder;
        $this->assertInstanceOf(RedisClientBuilder::class, $target);
        // This is a unit test; no connection should actually be established
        $result = $target->build([
            'host' => 'localhost',
            'port' => '6379',
            'password' => 'testy',
            'database' => 0,
        ], ClientWrapper::PHPREDIS_CLIENT_NAME);

        $this->assertInstanceOf(Client::class, $result);
    }
}
