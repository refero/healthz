FROM php:7.4.15-cli

RUN apt-get update
RUN apt-get install -y libzip-dev
RUN docker-php-ext-install zip

RUN pecl install -o redis-5.3.3
RUN rm -rf /tmp/pear
RUN docker-php-ext-enable redis

# Add a default user; not root 
RUN useradd phpcli -s /bin/bash -m -g root

# Install latest version of composer; may need to be restricted to a particular version eventually
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

USER phpcli

RUN mkdir -p /home/phpcli/.composer/cache

WORKDIR /home/phpcli
