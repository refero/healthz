<?php

namespace Healthz\Helpers;

use Healthz\Redis\Client;
use Healthz\Redis\ClientWrapper;

class RedisClientBuilder
{
    /**
     * Builds the Predis client object.
     *
     * Just abstracts away the instantiation from the Check itself.
     * Makes it alot easier to separate the External requirements from the logic.
     *
     * @param string $dsn
     * @param array  $config
     * @param string $client
     *
     * @return Client
     */
    public function build(array $config, string $client): ?Client
    {
        return new ClientWrapper($config, $client);
    }
}
