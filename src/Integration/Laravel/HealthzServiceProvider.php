<?php

namespace Healthz\Integration\Laravel;

use Healthz\Check\Configuration\ConfigurationBuilder;
use Healthz\CheckResolver;
use Illuminate\Support\ServiceProvider;

class HealthzServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config.php' => config_path('healthz.php'),
        ]);

        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*
         * This binding is provides alot of the core functionality for this package.
         * The CheckResolver is responsible for building the Check instances.
         *
         * Args:
         *  1: ConfigurationBuilder - handles building the configurations for the checks
         *  2: BuilderInterface[] - Objects responsible for building the checks you intend to use
         *  3: ConfigurationProviderInterface[] - Objects that can supply a config for a check
         */
        $this->app->bind(CheckResolver::class, function ($app) {
            $builders = array_map([$app, 'make'], config('healthz.builders'));
            $configurationProviders = array_map([$app, 'make'], config('healthz.configurationProviders'));
            return new CheckResolver(
                new ConfigurationBuilder,
                $builders,
                $configurationProviders
            );
        });
    }
}
