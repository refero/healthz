<?php

namespace Tests\Unit\Healthz;

use Healthz\CheckResolver;
use Healthz\Check\MysqlCheck;
use PHPUnit\Framework\TestCase;
use Healthz\Check\Builder\MysqlBuilder;
use Healthz\Check\Configuration\MysqlConfiguration;
use Healthz\Check\Configuration\ConfigurationBuilder;
use Healthz\Integration\Laravel\MysqlConfigurationProvider;

class CheckResolverTest extends TestCase
{
    public function testConstructionWithoutBuilders()
    {
        $mockConfigBuilder = $this->createMock(ConfigurationBuilder::class);
        
        $target = new CheckResolver($mockConfigBuilder, [], []);
        $this->assertInstanceOf(CheckResolver::class, $target);
    }

    public function testConstructionWithValidBuilders()
    {
        $mockConfigBuilder = $this->createMock(ConfigurationBuilder::class);
        $mockMysqlBuilder = $this->createMock(MysqlBuilder::class);

        $target = new CheckResolver($mockConfigBuilder, [$mockMysqlBuilder], []);
        $this->assertInstanceOf(CheckResolver::class, $target);
    }

    // public function testConstructionWithInvalidBuilders()
    // {
    //     // expect exception
    //     $mockConfigBuilder = $this->createMock(ConfigurationBuilder::class);
    //     $mockDateTime = $this->createMock(DateTime::class);

    //     $this->expectException(InvalidArgumentException::class);
    //     $this->expectExceptionMessage("Builder " . DateTime::class . " does not implement " . BuilderInterface::class);

    //     $target = new CheckResolver($mockConfigBuilder, [$mockDateTime], []);
    //     $this->assertInstanceOf(CheckResolver::class, $target);
    // }

    public function testConstructionWithValidConfigProvider()
    {
        $mockConfigBuilder = $this->createMock(ConfigurationBuilder::class);
        $mockMysqlProvider = $this->createMock(MysqlConfigurationProvider::class);

        $target = new CheckResolver($mockConfigBuilder, [], [$mockMysqlProvider]);
        $this->assertInstanceOf(CheckResolver::class, $target);
    }

    // public function testConstructionWithInvalidConfigProvider()
    // {
    //     // expect exception
    //     $mockConfigBuilder = $this->createMock(ConfigurationBuilder::class);
    //     $mockDateTime = $this->createMock(DateTime::class);

    //     $this->expectException(InvalidArgumentException::class);
    //     $this->expectExceptionMessage("Configuration Provider " . DateTime::class . " does not implement " . ConfigurationProviderInterface::class);

    //     $target = new CheckResolver($mockConfigBuilder, [], [$mockDateTime]);
    //     $this->assertInstanceOf(CheckResolver::class, $target);
    // }



/*
 * The below tests need to be implemented.
 * The problem we have is that the Resolver checks the class path of the builders 
 * and providers against the required classes.
 * We need to provide mocks meaning these class paths will not match, as the 
 * mock class won't have the same full class path as the concrete implementations.
 * We need to find a way to improve the logic for testing purposes without making 
 * it overly-complicated to extend this functionality. I want to avoid having a 
 * 'register' section which maps keys to class names, as they add alot of extra validation and user requirements without
 * adding much benefit to the library; it will actually make it more restrictive. 
 */




    // public function testResolvingFromOneBuilderAndConfigProvider()
    // {
    //     $input = [
    //         'class' => MysqlCheck::class,
    //         'configurationProvider' => [
    //             // 'class' => get class of mock,
    //             'arguments' => [
    //                 'connection' => 'mysql',
    //             ],
    //         ],
    //     ];

    //     $mockConfigBuilder = $this->createMock(ConfigurationBuilder::class);
    //     $mockConfigBuilder->expects($this->exactly(0))
    //         ->method('build');

    //     $mockConfig = $this->createMock(MysqlConfiguration::class);

    //     $mockMysqlProvider = $this->createMock(MysqlConfigurationProvider::class);
    //     $mockMysqlProvider->expects($this->exactly(1))
    //         ->method('retrieve')
    //         ->with($input['configurationProvider']['arguments'])
    //         ->willReturn($mockConfig);

    //     $input['configurationProvider']['class'] = get_class($mockMysqlProvider);
            
    //     $mockMysqlBuilder = $this->createMock(MysqlBuilder::class);

    //     $target = new CheckResolver($mockConfigBuilder, [$mockMysqlBuilder], [$mockMysqlProvider]);
    //     $this->assertInstanceOf(CheckResolver::class, $target);

    //     $result = $target->resolve($input);

    //     $this->assertInstanceOf(MysqlCheck::class, $result);
    // }

    // public function testResolvingFromOneBuilderAndConfigurationBlock()
    // {
        
    // }

    // public function testResolvingWithMultipleBuildersAndConfigurations()
    // {
        
    // }

    // public function testEcceptionIsThrownWithoutConfigurationBlock()
    // {

    // }

    // public function testExceptionIsThrownWithoutClassForConfigProvider()
    // {

    // }

    // public function testExceptionWhenBuilderIsNotRegistered()
    // {

    // }

    // public function testExceptionWhenConfigProviderIsNotRegistered()
    // {

    // }
}
