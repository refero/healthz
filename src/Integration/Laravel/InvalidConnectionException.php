<?php

namespace Healthz\Integration\Laravel;

use Exception;

class InvalidConnectionException extends Exception
{
}
