<?php

namespace Healthz\Integration\Laravel;

use Healthz\Check\Configuration\ConfigurationBuilder;
use Healthz\Check\Configuration\ConfigurationInterface;
use Healthz\Check\Configuration\RedisConfiguration;
use Healthz\Exception\UnknownRedisClientTypeException;
use Healthz\Integration\ConfigurationProviderInterface;
use Healthz\Redis\ClientWrapper;

class RedisConfigurationProvider implements ConfigurationProviderInterface
{
    /*
     * Retrieve the Configuration from the laravel config.
     * Using the laravel database.php config we extract the relevant credentials
     * for connecting to the Redis database.
     *
     * There is 1 acceptable argument, 'connection' which defines which of the
     * connections configured in database.php are used for the check. The arguments
     * passed here should be defined in the healthz config.
     */
    public function retrieve(array $arguments = []): ConfigurationInterface
    {
        $client = config('database.redis.client') ?? ClientWrapper::PHPREDIS_CLIENT_NAME;

        switch ($client) {
            case ClientWrapper::PREDIS_CLIENT_NAME:
                $redisParams = $this->extractPRedisParams($arguments);
                break;
            case ClientWrapper::PHPREDIS_CLIENT_NAME:
                $redisParams = $this->extractPhpRedisParams($arguments);
                break;
            default:
                throw new UnknownRedisClientTypeException("Redis client is not implemented '" . $client . "'.");
        }

        if ($redisParams === null) {
            throw new InvalidConnectionException("Could not find redis connection using arguments:  '" . json_encode($arguments, true) . "'.");
        }

        $redisParams['client'] = $client;
        $configBuilder = new ConfigurationBuilder;

        return $configBuilder->build(RedisConfiguration::class, $redisParams);
    }

    protected function extractPhpRedisParams(array $arguments): ?array
    {
        if ($arguments['cluster'] === true) {
            $connection = $arguments['connection'] ?? 'default';
            $index = $arguments['connection_index'] ?? 0;
            $redisParams = config("database.redis.clusters.$connection.$index");
        } else {
            $redisParams = $this->extractNonClusteredParams($arguments);
        }

        return $redisParams;
    }

    protected function extractPRedisParams(array $arguments): ?array
    {
        if ($arguments['cluster'] === true) {
            $connection = $arguments['connection'] ?? 'default';
            $redisParams = config('database.redis.clusters.' . $connection);
        } else {
            $redisParams = $this->extractNonClusteredParams($arguments);
        }

        return $redisParams;
    }

    protected function extractNonClusteredParams(array $arguments): ?array
    {
        $connection = $arguments['connection'] ?? config('database.redis.default');

        return config('database.redis.' . $connection);
    }
}
